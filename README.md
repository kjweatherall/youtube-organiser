# Youtube Organiser
## Introduction
YouTube Organiser is a website for organising your YouTube subscriptions by category, as well as queuing up videos to watch.
## Tech
The server for this site uses Node.js for the webserver, Vash for its templating, React for the site front-end and SQLite for its database.
## Settings.json
The Settings.json file is a JSON file that contains data that should not be included in the repo, e.g. API keys, as well as data that may differ across servers. Settings used by the server are:
- port: The port the server will listen to
- subserver: If the server is being used as a subserver. (See notes.)
- allowLogout: If the user can logout
## Running
1. Install Node.js if it is not already installed.
2. Install the necessary Node.js modules using "npm install" in the root folder of the project.
3. Download your credentials JSON file from the Google APIs site. (This contains data such as your Client ID and Client Secret.)
4. (Optional) Create a Settings.json file and set any desired settings inside of it.
5. Run using "npm start".
## Notes
- This site is currently in the prototype phase, so do not use it for production purposes.
- The server is currently being used like a normal server when testing, but is a sub-server for production. Basically, a main server handles traffic, and routes it to this server. (The main server handles multiple sub-servers.) When this server is being tested, it handles its own traffic. To deal with this, the settings has the key "subserver" that is used to indicate if the server is acting as a subserver or not. If it is a subserver, then it doesn't listen to a port and exports its Express app object.
- While the database has mostly been set up to handle multiple users, the site itself currently does not have a way to handle multiple users.
- It is currently not possible to subscribe/unsubscribe from the site itself. Subscriptions are still handled through YouTube.com with the account logged into the site.
- The server stores the authenication token in a file called "youtube-nodejs-quickstart.json" in "~/.credentials".