const sqlite3 = require('sqlite3').verbose(),
    express = require('express'),
    app = express(),
    YouTubeHelper = require('./custom_modules/YouTubeHelper'),
    DataHelper = require('./custom_modules/DataHelper'),
    path = require('path');

/////////////////////
///// VARIABLES /////
/////////////////////

/**
 * @type DataHelper helper
 */
let helper;
/**
 * @type YouTubeHelper ytHelper
 */
let ytHelper;

////////////////////
///// SETTINGS /////
////////////////////

const defaults = {
    subserver:false,
    port:3010,
    allowLogout:true
};

let settings = require('./custom_modules/Settings')('../Settings.json', defaults);

////////////////////
///// DATABASE /////
////////////////////

const db = new sqlite3.Database(path.join(__dirname, 'data.db'), function(err) {
    if (err) {
        console.error(`Error connecting to database: ${err}`);
    } else {
        db.run(`CREATE TABLE IF NOT EXISTS users(name TEXT, username TEXT, password TEXT, salt TEXT, last_updated INTEGER DEFAULT 0)`, err=>{
            if (err) {
                console.error(`Error creating table users: ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS categories(title TEXT, user INTEGER, last_updated TEXT, parentID INTEGER, updates INTEGER)`, err=>{
            if (err) {
                console.error(`Error creating table categories: ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS categories_channels(categoryID INTEGER, channelID INTEGER)`, err=>{
            if (err) {
                console.error(`Error creating table category_channels: ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS channels(title TEXT, channelID TEXT, url TEXT, last_updated TEXT, thumbnail TEXT, updateEnabled INTEGER DEFAULT 1)`, err=>{
            if (err) {
                console.error(`Error creating table channels: ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS videos(title TEXT, description TEXT, url TEXT UNIQUE, publishedAt TEXT, channelID TEXT, thumbnail TEXT)`, err=>{
            if (err) {
                console.error(`Error creating table videos: ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS user_subs(user INTEGER, channelID TEXT, unwatched INTEGER DEFAULT 0)`, err=>{
            if (err) {
                console.error(`Error creating table user_subs: ${err}`);
            }
        });

        db.run(`CREATE TABLE IF NOT EXISTS queue(qID INTEGER PRIMARY KEY, userID INTEGER, videoID TEXT)`, err=>{
            if (err) {
                console.error(`Error creating table queue: ${err}`);
            }
        });
    }

    ytHelper = new YouTubeHelper(db, settings);
    helper = new DataHelper(db);
});

///////////////////
///// EXPRESS /////
///////////////////

// set up views
app.set('view engine', 'vash');
app.set('views', path.join(__dirname, 'views'));

// set up public folders
app.use(express.static(path.join(__dirname, 'public')));

if (!settings.obfuscate) {
    app.use(express.static(path.join(__dirname, 'scripts')));
} else {
    app.use(express.static(path.join(__dirname, 'scripts-obfuscated')));
}

// for receiving JSON
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// start listening
if (!settings.subserver) {
    app.listen(settings.port, _=>console.log(`Connected on port ${settings.port}...`));
}

/////////////////////
///// ENDPOINTS /////
/////////////////////

app.get('/', (req, res)=>{
    if (!ytHelper.isReady()) {
        res.send(`<html><body><h1>Server still loading! Try refreshing!</h1></body></html>`);
    } else {
        res.render('index', {loggedIn:ytHelper.isLoggedIn()});
    }
});

app.get('/login', (req, res)=>{
    ytHelper.authorize().then(ret=>{
        if (!ret.status) {
            res.redirect('/');
        } else {
            res.redirect(ret.url);
        }
    });
});

app.get('/logout', (req, res)=>{
    if (settings.allowLogout) {
        ytHelper.logout().then(_=>{
            res.redirect('/');
        }).catch(err=>{
            res.redirect('/');
        });
    } else {
        res.redirect('/');
    }
});

// callback for logging into youtube
app.get('/oauth2callback', (req, res)=>{
    ytHelper.oauth2callback(req.query.code)
    .then(()=>res.redirect('/'))
    .catch(()=>res.end('Server error'));
});

app.get(['/api/v1/:type','/api/v1/:type/:subtype'], (req, res)=>{
    let type = req.params.type,
        subtype = req.params.subtype || null;

    switch (type) {
        case "subscriptions":
            switch (subtype) {
                case "force":
                    ytHelper.updateSubs()
                    .then(()=>{
                        // Note: Function to sort subs is inside helper, so we use this function to get a sorted list,
                        // but is there a better way? Do we just expose the function inside of DataHelper?
                        helper.getSubs()
                        .then(subs=>res.json({error:0,subs:subs}))
                        .catch(err=>{
                            console.log(`Error getting subs: ${err}`);
                            res.json({error:1,errorMsg:err});
                        });
                    })
                    .catch(err=>res.json({error:1,errorMsg:err}));
                    return;
                case null:
                    helper.getSubs()
                    .then(subs=>{
                        res.json({error:0,subs:subs});
                    })
                    .catch(err=>{
                        console.log(`Error: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    return;
            }
            break;
        case "channel":
            switch (subtype) {
                case null:
                    if (req.query.id) {
                        helper.getChannel(req.query.id)
                        .then(channel=>res.json({error:0,channel:channel}))
                        .catch(err=>res.json({error:1,errorMsg:err}));
                    } else {
                        res.json({error:1,errorMsg:"No channel ID specified."});
                    }
                    return;
            }        
            break;
        case "category-videos":
            switch (subtype) {
                case null:
                    helper.getCategoryVideos(req.query.id, req.query.offset, req.query.results)
                    .then(videos=>res.json({error:0,videos:videos}))
                    .catch(err=>{
                        console.log(`Error getting videos for category: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    return;
            }            
            return;
        case "videos":
            switch (subtype) {
                case null:
                    let id = req.query.id,
                        max = req.query.max;
                    if (id) {
                        helper.getVideosForChannel(id, max)
                        .then(videos=>{
                            res.json({error:0,videos:videos});
                        })
                        .catch(err=>{
                            console.error(`Error getting videos: ${err}`);
                            res.json({error:1,errorMsg:err});
                        });
                    } else {
                        let errMsg = "No channel ID specified.";
                        res.json({error:1,errorMsg:errMsg});
                    }
                    return;
            }
            return;
        case "user":
            switch(subtype) {
                case "updates":
                    helper.getUpdates()
                    .then(updates=>res.json({error:0,updates:updates}))
                    .catch(err=>{
                        console.error(`Error getting user updates: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    return;
                case null:
                    helper.getUserData()
                    .then(data=>{
                        helper.getSubs()
                        .then(subs=>{
                            data.subs = subs;
                            data.allowLogout = settings.allowLogout;
                            res.json({error:0,data:data});
                        })
                        .catch(err=>{
                            console.error(`Error getting subs: ${err}`);
                            res.json({error:1,errorMsg:"Error getting subs"});
                        });
                    })
                    .catch(err=>{
                        console.log(`Error getting user data: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    return;
            }            
            return;
        case "updates":
            switch (subtype) {
                case null:
                    let after = req.query.after;
                    if (!after) {
                        res.json({error:1,errorMsg:"Not after time specified."});
                    } else {
                        helper.getUpdates(after)
                        .then((data)=>{
                            res.json({error:0,updates:data.updates,lastUpload:data.lastUpload});
                        })
                        .catch(err=>{
                            console.error(`Error getting updates: ${err}`);
                            res.json({error:1,errorMsg:err});
                        });
                    }
                    return;
            }            
            return;
        case "force-update":
            switch(subtype) {
                case null:
                    ytHelper.forceUpdate();
                    res.json({error:0});
                    return;
            }            
            return;
        case "queue":
            switch (subtype) {
                case null:
                    helper.getQueue()
                    .then(videos=>res.json({error:0,videos:videos}))
                    .catch(err=>res.json({error:1,errorMsg:err}));
                    return;
            }            
            return;
    }

    if (subtype) {
        res.json({error:1,errorMsg:`Unknown subtype "${subtype} for type ${type}"`});
    } else {
        res.json({error:1,errorMsg:`Unknown type "${type}"`});
    }
});

app.post('/api/v1/:type/:subtype', (req, res)=>{
    switch(req.params.type) {
        case "categories":
            if (req.params.subtype === "add") {
                if (req.body.title) {                
                    helper.addCategory(req.body.title, req.body.parentID)
                    .then(category=>{
                        res.json({error:0,category:category});
                    })
                    .catch(err=>{
                        console.error(`Error: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                } else {
                    res.json({error:1,errorMsg:"No title specified for category."});                    
                }
                return;
            } else if (req.params.subtype === "add-channel") {
                let id = req.body.id;
                let channel = req.body.channel;
                if (!id || !channel) {
                    res.json({error:1,errorMsg:(!id && !channel) ? "Category and channel not specified" : !id ? "Category not specified" : "Channel not specified"});
                } else {
                    helper.addChannelToCategory(id, channel)
                    .then(_=>res.json({error:0}))
                    .catch(err=>res.json({error:1,errorMsg:err}));
                    
                }
                return;
            } else if (req.params.subtype === "remove-channel") {
                let id = req.body.id;
                let channel = req.body.channel;
                if (!id || !channel) {
                    res.json({error:1,errorMsg:(!id && !channel) ? "Category and channel not specified" : !id ? "Category not specified" : "Channel not specified"});
                } else {
                    helper.removeChannelFromCategory(id, channel)
                    .then(_=>res.json({error:0}))
                    .catch(err=>res.json({error:1,errorMsg:err}));
                }
                return;
            } else if (req.params.subtype === "remove") {
                if (req.body.id) {
                    helper.removeCategory(req.body.id)
                    .then(_=>res.json({error:0}))
                    .catch(err=>res.json({error:1,errorMsg:err}));
                } else {
                    res.json({error:1,errorMsg:"No category ID specified"});
                }
                return;
            }
            
            res.json({error:1,errorMsg:"No subtype specified"});
            return;
        case "updates":
            switch(req.params.subtype) {
                case "clear":
                    let id = req.body.id;
                    helper.clearUpdates(id)
                    .then(_=>{
                        res.json({error:0});
                    })
                    .catch(err=>{
                        console.log(`Error clearing updates: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    break;
                default:
                    res.json({error:1,errorMsg:"No subtype specified"});
                    break;
            }
            break;
        case "queue":
            switch(req.params.subtype) {
                case "add":{
                    let id = req.body.videoID;
                    helper.addToQueue(id)
                    .then(qID=>{
                        res.json({error:0,data:{qID:qID}});
                    })
                    .catch(err=>{
                        console.error(`Error adding to queue: ${err}`);
                        res.json({error:1, errorMsg:err});
                    });
                    break;
                }
                case "clear":
                    helper.clearQueue()
                    .then(_=>{
                        res.json({error:0});
                    })
                    .catch(err=>{
                        console.error(`Error clearing queue: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    break;
                case "remove":
                    let qID = req.body.qID;
                    helper.removeFromQueue(qID)
                    .then(_=>{
                        res.json({error:0});
                    })
                    .catch(err=>{
                        console.error(`Error removing from queue: ${err}`);
                        res.json({error:1,errorMsg:err});
                    });
                    break;
                default:
                    res.json({error:1,errorMsg:"No subtype specified"});
                    break;
            }
            break;
        default:
            res.json({error:1,errorMsg:"No type specified"});
            break;
    }
});

if (settings.subserver) {
    exports.app = app;
}