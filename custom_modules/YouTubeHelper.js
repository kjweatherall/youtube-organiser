const fs = require('fs'),
    {google} = require('googleapis'),
    readJSON = require('r-json');

//////////////////
///// CONSTS /////
//////////////////

const SCOPES = ['https://www.googleapis.com/auth/youtube.readonly'];

// TODO: Put in settings
const TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';
const TOKEN_PATH = TOKEN_DIR + 'youtube-nodejs-quickstart.json';

const OAuth2 = google.auth.OAuth2;
const service = google.youtube('v3');

/////////////////////
///// VARIABLES /////
/////////////////////

let CLIENT_ID = null, CLIENT_SECRET = null, REDIRECT_URL = null;

// if youtube data has been loaded
let youtubeData = false;

// youtube auth token
let TOKEN = null;

if (fs.existsSync(TOKEN_PATH)) {
    TOKEN = JSON.parse(fs.readFileSync(TOKEN_PATH));
}

/////////////////////
///// FUNCTIONS /////
/////////////////////

// creates token folder if necessary and then calls function to save token to file
function storeToken(token) {
    return new Promise((resolve, reject)=>{
        TOKEN = token;

        fs.exists(TOKEN_DIR, exists=>{
            if (!exists) {
                fs.mkdir(TOKEN_DIR, err=>{
                    if (err) {
                        console.error(`Error creating token folder: ${err}`);
                        reject(err);
                    } else {
                        writeTokenToFile(JSON.stringify(token))
                        .then(()=>resolve())
                        .catch(err=>reject(err));
                    }
                });
            } else {
                writeTokenToFile(JSON.stringify(token))
                .then(()=>resolve())
                .catch(err=>reject(err));
            }                
        });
    });        
}

// generates OAuth object used for YouTube requests
// TODO: Create just a single re-useable object
function generateOAuth() {
    if (!youtubeData || !CLIENT_ID || !CLIENT_SECRET || !REDIRECT_URL) {
        if (!youtubeData) {
            console.error(`YouTube data not let loaded`);
        } else {
            console.error(`YouTube data missing.`);
        }
        return null;
    }

    let oauth = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
    oauth.credentials = TOKEN;
    return oauth;
}

// writes token to file
function writeTokenToFile(data) {
    return new Promise((resolve, reject)=>{
        fs.writeFile(TOKEN_PATH, data, (err)=>{
            if (err) {
                console.error(`Error writing token to file: ${err}`);
                reject();
            } else {
                console.log(`Token stored to ${TOKEN_PATH}`);
                resolve();
            }
        });
    });
}

// loads YouTube authorisation data from file
function loadData() {
    let path = require('path');

    readJSON(path.join(__dirname, '../client_id.json'), (err, data)=>{
        if (err) {
            console.error(`Error loading YouTube data.`);        
        } else {
            CLIENT_SECRET = data.web.client_secret;
            CLIENT_ID = data.web.client_id;
            REDIRECT_URL = data.web.redirect_uris[0];
            youtubeData = true;
        }
    });
}

// calls YouTube API
function callAPI(type, data, auth = null) {
    return new Promise(function(resolve, reject) {
        if (!auth) {
            auth = generateOAuth();
        }

        data.auth = auth;

        if (!data.maxResults) {
            data.maxResults = 50;
        }

        service[type].list(data, function(err, response) {
            if (err) {
                console.error(`Error calling API for ${type}: ${err} (${err.response.data.error_description})`);
                reject(err);
            } else {
                resolve(response);
            }
        });
    });
}

// fetches data for specified channel from YouTube
function downloadChannel(id) {
    return new Promise((resolve, reject)=>{
        let data = {
            part:'snippet',
            id:id
        };
        callAPI('channels', data)
            .then(response=>{
                let item = response.data.items[0];
                if (!item) {
                    resolve(null);
                } else {
                    let thumbnail = null;
                    let thumbnails = response.snippet.thumbnails;
                    if (thumbnails.high) {
                        thumbnail = thumbnails.high.url;
                    } else if (thumbnails.medium) {
                        thumbnail = thumbnails.medium.url;
                    } else if (thumbnails.default) {
                        thumbnail = thumbnails.default.url;
                    }

                    let channel = {
                        title:item.snippet.title,
                        description:item.snippet.description,
                        thumbnail:thumbnail
                    };
                    resolve(channel);
                }
            })
            .catch(err=>reject(err));
    });
}

// fetches videos for specified channel
function downloadVideosForChannel(id, after = null) {
    return new Promise((resolve, reject)=>{
        let data = {
            part:'snippet,contentDetails',
            channelId:id
        };

        if (after) {
            data.publishedAfter = after;
        }

        callAPI("activities", data)
            .then(response=>{
                // reduce the data to just the properties we need
                let videos = response.data.items.map(i=>{
                    if (i.snippet.type === "upload" || i.snippet.type === "playlistItem") {
                        let thumbnail = null;

                        // chooses the highest res thumbnail available
                        let thumbnails = i.snippet.thumbnails;
                        if (thumbnails.maxres) {
                            thumbnail = thumbnails.maxres.url;
                        } else if (thumbnails.standard) {
                            thumbnail = thumbnails.standard.url;
                        } else if (thumbnails.high) {
                            thumbnail = thumbnails.high.url;
                        } else if (thumbnails.medium) {
                            thumbnail = thumbnails.medium.url;
                        } else if (thumbnails.default) {
                            thumbnail = thumbnails.default.url;
                        }

                        // get url; "upload" and "playlistItem" use different data structure for this
                        // TODO: Make sure data doesn't have videoID in a location shared by both types
                        let url;
                        if (i.contentDetails.upload) {
                            url = i.contentDetails.upload.videoId;
                        } else {
                            url = i.contentDetails.playlistItem.resourceId.videoId;
                        }

                        return {
                            title:i.snippet.title,
                            publishedAt:i.snippet.publishedAt,
                            channelID:i.snippet.channelId,
                            description:i.snippet.description,
                            thumbnail:thumbnail,
                            type:i.snippet.type,
                            url:url
                        };
                    } else {
                        return null;
                    }
                });

                // remove any invalid entries
                for (let i = 0; i < videos.length; i++) {
                    if (!videos[i]) {
                        videos.splice(i--, 1);
                    }
                }

                resolve(videos);
            })
            .catch(err=>reject(err));            
    });
}

// fetches user's subs; technically a recursive method, as each call can receive a page token, causing the 
// function to call itself with the new token
function downloadSubs(auth = null, pageToken = null) {
    if (!auth) {
        auth = generateOAuth();
    }

    return new Promise((resolve, reject)=>{
        let data = {
            mine:true,                
            part:'snippet,contentDetails'
        };

        if (pageToken) {
            data.pageToken = pageToken;
        }

        callAPI('subscriptions',data,auth)
            .then(response=>{
                // reduce to just the properties we need
                let subs = response.data.items.map(s=>{
                    let thumbnail = null;

                    // choose the highest res thumbnail
                    let thumbnails = s.snippet.thumbnails;
                    if (thumbnails.high) {
                        thumbnail = thumbnails.high.url;
                    } else if (thumbnails.medium) {
                        thumbnail = thumbnails.medium.url;
                    } else if (thumbnails.default) {
                        thumbnail = thumbnails.default.url;
                    }

                    return {                        
                        title:s.snippet.title,
                        thumbnail:thumbnail,
                        channelID:s.snippet.resourceId.channelId
                    };
                });

                // call function with page token for next page if one was given
                if (response.data.nextPageToken) {
                    downloadSubs(auth, response.data.nextPageToken)
                    .then(newSubs=>{
                        subs = subs.concat(newSubs);
                        resolve(subs);
                    })
                    .catch(err=>reject(err));
                } else {
                    resolve(subs);
                }
            })
            .catch(err=>reject(err));
    });
}

// fetches videos for channel and caches results
function downloadAndCacheVideosForChannel(id, db, after = null) {
    return new Promise((resolve, reject)=>{
        downloadVideosForChannel(id, after)
        .then(videos=>{
            db.parallelize(()=>{
                // update time/date when channel was updated
                db.run(`UPDATE channels SET last_updated=? WHERE channelID=?`,
                    [(new Date()).toISOString(), id],
                    function(err) {
                        if (err) {
                            console.error(`Error updating channel ${id}: ${err}`);
                        }
                    });
                
                // increase number of unwatched videos for channel
                if (videos.length > 0) {
                    db.run(`UPDATE user_subs SET unwatched=unwatched+? WHERE channelID=?`,
                        [videos.length,id],
                        function(err) {
                            if (err) {
                                console.error(`Error increasing updates count: ${err}`);
                            }
                        });
                }

                // insert videos
                videos.forEach(v=>{
                    db.run(`INSERT OR IGNORE INTO videos(title,description,url,publishedAt,channelID,thumbnail) VALUES(?,?,?,?,?,?)`,
                    [
                        v.title,
                        v.description,
                        v.url,
                        v.publishedAt,
                        id,
                        v.thumbnail
                    ],
                    function(err) {
                        if (err) {
                            console.error(`Error caching video ${v.url} for channel ${id}: ${err}`);
                        }
                    });
                });
            });

            resolve(videos);
        })
        .catch(err=>reject(err));
    });
}

// fetches subs for channel and caches results
function downloadAndCacheSubs(db) {
    return new Promise((resolve, reject)=>{
        downloadSubs()
        .then(subs=>{
            // promise is resolved when all subs have been handled
            let count = 0;
            let check = ()=>{
                if (++count === subs.length) {
                    resolve(subs);
                }
            };

            db.parallelize(()=>{
                // update time/date user was updated
                db.run(`UPDATE users SET last_updated=? WHERE rowid=1`,
                    (new Date()).toISOString(),
                    function(err) {
                        if (err) {
                            console.error(`Error updating user: ${err}`);
                        }
                    });

                subs.forEach((s,i)=>{
                    // subs user to channel if not already subbed
                    db.get(`SELECT * FROM user_subs WHERE channelID=?`, s.channelID, (err, row)=>{
                        if (err) {
                            console.err(`Error getting channel ${s.channelID} from user_subs: ${err}`);
                        } else {
                            if (!row) {
                                db.run(`INSERT INTO user_subs(user,channelID) VALUES (1,?)`,
                                s.channelID,
                                function(err) {
                                    if (err) {
                                        console.error(`Error inserting user sub ${s.channelID}: ${err}`);
                                    }
                                });
                            }
                        }
                    });
                    
                    // adds channel if not already added
                    db.get(`SELECT * FROM channels WHERE channelID=?`, s.channelID, (err, row)=>{
                        if (err) {
                            console.err(`Error getting channel ${s.channelID} from channels: ${err}`);
                            check();
                        } else {
                            if (!row) {
                                subs[i].updateEnabled = 1;

                                db.run(`INSERT INTO channels(title,channelID,thumbnail) VALUES (?,?,?)`,
                                    [s.title,s.channelID,s.thumbnail],
                                    function(err) {
                                        if (err) {
                                            console.error(`Error inserting channel: ${err}`);
                                        }
                                        check();
                                    });
                            } else {
                                subs[i].updateEnabled = row.updateEnabled;
                                subs[i].last_updated = row.last_updated;
                                check();
                            }
                        }
                    });
                });
            });
        })
        .catch(err=>reject(err));
    });
}

// gets channel information if stored, or fetches it, stores it and then returns it
function initChannel(id, db) {
    return new Promise((resolve, reject)=>{
        db.get('SELECT * FROM channels WHERE channelID=?', id, (err, row)=>{
            if (err) {
                console.error(`Error getting channel: ${err}`);
                reject(err);
            } else {
                if (!row) {
                    downloadChannel(id)
                    .then(channel=>{
                        db.run(`INSERT INTO channels(title,channelID,last_updated) VALUES(?,?,?)`,
                            [channel.title,id,(new Date()).toISOString()],
                            err=>{
                                if (err) {
                                    console.error(`Error inserting channel ${id}: ${err}`);
                                    reject(err);
                                } else {
                                    resolve(channel);
                                }
                            });
                    })
                    .catch(err=>reject(err));
                } else {
                    resolve(row);
                }
            }
        });
    });
}

// recursive function the fetches and caches new videos for a list of channels
function updateSubs(subs, db, idx = 0) {
    return new Promise((resolve, reject)=>{
        if (idx >= subs.length) {
            resolve();
        } else {
            console.log(`${idx+1}/${subs.length}`);

            downloadAndCacheVideosForChannel(subs[idx].channelID, db, subs[idx].last_updated)
            .then(()=>{
                updateSubs(subs, db, idx + 1)
                .then(()=>resolve())
                .catch(err=>resolve());
            })
            .catch(err=>{
                // TODO: Why does this resolve and not reject?
                console.error(`Error downloading videos for channel ${subs[idx].title} (${subs[idx].channelID}): ${err}`);
                updateSubs(subs, db, idx + 1)
                .then(()=>resolve())
                .catch(err=>resolve());
            });
        }
    });
}

// updates YouTube data
function update(db) {
    console.log(`Updating...`);

    downloadAndCacheSubs(db)
    .then(subs=>{
        let s = [ ];
        subs.forEach(sub=>{
            if (sub.updateEnabled) {
                s.push(sub);
            }
        });
        updateSubs(s, db).then(()=>console.log(`Done updating!`));
    })
    .catch(err=>{
        console.error(`Error downloading and caching subs: ${err}`);
    });
}

//////////////////
///// CLASS //////
//////////////////

module.exports = class {
    constructor(db, settings = { }) {
        this.db = db;
        this.timer = null;
        this.updateFrequency = (settings.updateFrequency || 15) * (1000 * 60);

        loadData();
        this.startTimer();
    }

    // starts timer for fetching data from YouTube
    startTimer() {
        if (!this.timer) {
            this.timer = setInterval(_=>update(this.db), this.updateFrequency);
        }
    }

    // stops timer for fetching data from YouTube
    stopTimer() {
        if (this.timer) {            
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    // if YouTube ready to use
    isReady() {
        return youtubeData;
    }

    // if logged in
    isLoggedIn() {
        return TOKEN !== null;
    }

    // produces authorisation URL
    authorize() {
        // TODO: This doesn't appear to be asynchronous. Does it need to use a promise?
        return new Promise((resolve, reject)=>{
            const oauth2Client = generateOAuth();        
            const authUrl = oauth2Client.generateAuthUrl({
                access_type:'offline',
                scope:SCOPES
            });
            resolve({status:1,url:authUrl});
        });
    }

    // logs out
    logout() {
        return new Promise((resolve, reject)=>{
            TOKEN = null;
            fs.unlink(TOKEN_PATH, err=>{
                if (err) {
                    console.error(`Error logging out: ${err}`);
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    // callback after logging into YouTube
    oauth2callback(code) {
        return new Promise((resolve, reject)=>{
            let oauth2Client = generateOAuth();
            oauth2Client.getToken(code, (err, token)=>{
                if (err) {
                    console.error(`Error while tring to retrieve access token: ${err}`);
                    reject(err);
                } else {
                    storeToken(token)
                    .then(()=>resolve())
                    .catch(err=>reject(err));
                }
            });
        });
    }

    // forces immediate update of data
    forceUpdate() {
        this.stopTimer();
        update(this.db);
        this.startTimer();
    }

    // updates and returns subs
    updateSubs() {
        return downloadAndCacheSubs(this.db);
    }
};