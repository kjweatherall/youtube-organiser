const fs = require('fs');
const path = require('path');

// loads settings from specified JSON file. if defaults specified, adds values into settings object
module.exports = function(filename, defaults = null) {    
    let settings = { };

    // load settings
    let fn = path.join(__dirname, filename);
    if (fs.existsSync(fn)) {
        let data = fs.readFileSync(fn, 'utf8');
        settings = JSON.parse(data);
    } else {
        console.log(`No settings file!`);
    }

    // add missing defaults
    if (defaults) {
        for (let i in defaults) {
            if (typeof settings[i] === "undefined") {
                settings[i] = defaults[i];
            }
        }
    }

    return settings;
};