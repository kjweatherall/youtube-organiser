/* DataHelper houses the functions for interacting with the local database. */

// sorts a list in alphabetical order, ignore a preceding 'the'
function sortSubs(list) {
    return list.sort((a,b)=>{
        let titleA = a.title,
            titleB = b.title;
        if (titleA.substring(0, 4).toLowerCase() === 'the ') {
            titleA = titleA.substring(4);
        }
        if (titleB.substring(0, 4).toLowerCase() === 'the ') {
            titleB = titleB.substring(0, 4);
        }
        return titleA.toLowerCase().localeCompare(titleB.toLowerCase());
    });
}

module.exports = class {
    constructor(db) {
        if (db) {
            this.db = db;
        } else {
            console.error(`No database provided to DataHelper`);
        }
    }

    // adds a category with the chosen title and parent
    addCategory(title, parentID) {
        let db = this.db;

        return new Promise(function(resolve, reject) {
            db.run(`INSERT INTO categories(title, user, parentID) VALUES(?,1,?)`,
                [title,parentID],
                function(err) {
                    if (err) {
                        console.error(`Error adding category ${title}: ${err}`);
                        reject(err);
                    } else {
                        resolve({title:title,categoryID:this.lastID,channels:[],parentID:parentID});
                    }
                });
        });
    }

    // removes the specified category
    removeCategory(categoryID) {
        return new Promise((resolve, reject)=>{
            // removes all subcategories of this category
            this.db.each(`SELECT rowid FROM categories WHERE parentID=?`, categoryID, (err, row)=>{
                if (err) {
                    console.log(`Error getting subcategories: ${err}`);
                } else {
                    this.db.run(`DELETE FROM categories WHERE rowid=?`, row.rowid, (err)=>{
                        if (err) {
                            console.error(`Error deleting subcategory: ${err}`);
                        } 
                    });
                    this.db.run(`DELETE FROM categories_channels WHERE rowid=?`, row.rowid, (err)=>{
                        if (err) {
                            console.error(`Error deleting subcategory: ${err}`);
                        }
                    });
                }
            });
    
            // removes category
            this.db.run(`DELETE FROM categories WHERE rowid=?`, categoryID, (err)=>{
                if (err) {
                    console.error(`Error removing category: ${err}`);
                    reject(err);
                } else {
                    this.db.run(`DELETE FROM categories_channels WHERE categoryID=?`, categoryID, (err)=>{
                        if (err) {
                            console.error(`Error deleting category-channel connection: ${err}`);
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                }
            });
        });
    }

    // recursive function that takes a list of category objects and fills their "channels" array with the channels
    // for that category
    // TODO: See if this can be done without recursion
    fillCategories(categories, idx = 0) {
        return new Promise((resolve, reject)=>{
            if (idx >= categories.length) {
                resolve();
            } else {
                this.db.all(`SELECT channelID FROM categories_channels WHERE categoryID=?`,
                    categories[idx].rowid,
                    (err,rows)=>{
                        if (err) {
                            console.error(`Error filling category ${categories[idx].rowid}: ${err}`);
                            reject(err);
                        } else {
                            categories[idx].channels = categories[idx].channels.concat(rows.map(r=>r.channelID));
    
                            this.fillCategories(categories, idx + 1)
                            .then(_=>resolve())
                            .catch(err=>reject(err));
                        }
                    });
            }
        });
    }

    // gets all the categories
    getCategories() {
        return new Promise((resolve, reject)=>{
            this.db.all(`SELECT rowid,title,last_updated,parentID FROM categories WHERE user=1`,
                (err, categories)=>{
                    if (err) {
                        console.log(`Error getting categories: ${err}`);
                        reject(err);
                    } else {
                        for (let i of categories) {
                            i.channels = [ ];                        
                        }
                        this.fillCategories(categories)
                        .then(_=>{
                            for (let i of categories) {
                                i.categoryID = i.rowid;
                                delete i.rowid;
                            }

                            resolve(categories.sort((a,b)=>a.title.localeCompare(b.title)));
                        })
                        .catch(err=>reject(err));
                    }
                });
        });
    }

    // adds the specified channel to the specified category
    addChannelToCategory(categoryID, channelID) {
        return new Promise((resolve, reject)=>{
            this.db.run(`INSERT INTO categories_channels(categoryID,channelID) VALUES(?,?)`,
                [categoryID, channelID],
                err=>{
                    if (err) {
                        console.error(`Error adding channel to category: ${err}`);
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }

    // removes the specified channel from the specified category
    removeChannelFromCategory(categoryID, channelID) {
        return new Promise((resolve, reject)=>{
            this.db.run(`DELETE FROM categories_channels WHERE categoryID=? AND channelID=?`,
                [categoryID, channelID],
                (err)=>{
                    if (err) {
                        console.error(`Error removing channel ${channelID} from category ${categoryID}: ${err}`);
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }

    // gets videos for the specified category
    getCategoryVideos(categoryID, offset = 0, results = 20) {
        if (!categoryID) {
            reject(`ID not specified`);
        } else {
            return new Promise((resolve, reject)=>{
                // get category and its subcategories
                this.db.all(`SELECT rowid FROM categories WHERE rowid=? OR parentID=?`, [categoryID, categoryID], (err,cats)=>{
                    let categories = cats.map(r=>r.rowid);
                    let catInject = cats.map(_=>'?').join(',');
        
                    // get channels for categories
                    this.db.all(`SELECT channelID FROM categories_channels WHERE categoryID IN (${catInject})`, categories, (err, rows)=>{
                        if (err) {
                            console.error(`Error getting channels in category ${categoryID}: ${err}`);
                            reject();
                        } else {
                            let channels = rows.map(r=>r.channelID);
                            let inject = rows.map(_=>'?').join(',');
                
                            channels.push(offset);
                            channels.push(results);
        
                            // get videos for channels
                            this.db.all(`SELECT * FROM videos WHERE channelID IN (${inject}) ORDER BY publishedAt DESC LIMIT ?,?`, channels, (err, videos)=>{
                                if (err) {
                                    console.error(`Error getting videos for channels: ${err}`);
                                    reject(err);
                                } else {
                                    resolve(videos);
                                }
                            });
                        }
                    });
                });
            });
        }
    }

    // gets count of unwatched videos for each sub
    getUpdates() {
        return new Promise((resolve, reject)=>{
            this.db.all(`SELECT channelID,unwatched FROM user_subs WHERE user=1`,(err, rows)=>{
                if (err) {
                    reject(`Error getting updates: ${err}`);
                } else {
                    resolve(rows);
                }
            });
        });
    }

    // gets queue
    getQueue() {
        return new Promise((resolve, reject)=>{
            this.db.all(`SELECT queue.videoID as id,channels.title AS channel,videos.title AS title,qID FROM videos INNER JOIN queue ON queue.videoID=videos.url INNER JOIN channels ON channels.channelID=videos.channelID WHERE userID=1 ORDER BY qID`,
                (err, videos)=>{
                    if (err) {
                        console.error(`Error getting queue: ${err}`);
                        reject(err);
                    } else {
                        resolve(videos);
                    }
                }
            );
        });
    }

    // gets all user data needed to initiate app
    getUserData() {
        return new Promise((resolve, reject)=>{
            this.getCategories()
            .then(categories=>{
                this.getUpdates()
                .then(updates=>{
                    this.getQueue()
                    .then(queue=>{
                        this.db.get(`SELECT publishedAt FROM videos ORDER BY publishedAt DESC LIMIT 1`, (err, row)=>{
                            if (err) {
                                console.error(`Error getting publishedAt for video: ${err}`);
                                reject(err);
                            } else {
                                let lastUpload;

                                if (row) {
                                    lastUpload = row.publishedAt;
                                } else {
                                    lastUpload = (new Date()).toISOString();
                                }

                                let data = {
                                    categories:categories,
                                    updates:updates,
                                    queue:queue,
                                    lastUpload:lastUpload
                                };

                                resolve(data);
                            }
                        });
                    })
                    .catch(err=>{
                        reject(err);
                    });
                })
                .catch(err=>{
                    reject(err);
                });                    
            })
            .catch(err=>{
                reject(err);
            });
        });
    }

    // gets subs
    getSubs() {
        return new Promise((resolve, reject)=>{
            this.db.all(`SELECT title,channels.channelID,last_updated,thumbnail,unwatched,unwatched FROM channels INNER JOIN user_subs ON user_subs.channelID = channels.channelID WHERE user=1`,
                (err, rows)=>{
                    if (err) {
                        console.error(`Error getting subs: ${err}`);
                        reject(err);
                    } else {
                        resolve(sortSubs(rows));
                    }
                });
        });
    }

    // clears updates for channels in specified category
    clearUpdates(categoryID) {
        return new Promise((resolve, reject)=>{
            if (!categoryID) {
                reject(`No ID specified for clearing updates`);
            } else {
                this.db.run(
                    `update user_subs set unwatched=0 where channelID in (select categories_channels.channelID from categories_channels inner join categories on categories_channels.categoryID=categories.rowid where categories.rowid=? OR categories.parentID=?)`,
                    [categoryID, categoryID],
                    function(err) {
                        if (err) {
                            reject(`Error clearing updates: ${err}`);
                        } else {
                            resolve();
                        }
                    }
                );
            }
        });
    }

    // adds video to queue
    addToQueue(videoID) {
        return new Promise((resolve, reject)=>{
            if (!videoID) {
                reject(`ID not provided for adding to queue`);
            } else {
                this.db.run(`INSERT INTO queue(userID,videoID) VALUES(1,?)`, [videoID], function(err) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(this.lastID);
                    }
                });
            }
        });
    }

    // removes a video from the queue
    removeFromQueue(qID) {
        return new Promise((resolve, reject)=>{
            if (!qID) {
                reject(`No qID specified for removing from queue.`);
            } else {
                this.db.run(`DELETE FROM queue WHERE qID=? AND userID=1`, qID, function(err) {
                    if (err) {
                        reject(`Error removing queue item ${qID} from queue: ${err}`);
                    } else {
                        resolve();
                    }
                });
            }
        });
    }

    // clears queue
    clearQueue() {
        return new Promise((resolve, reject)=>{
            this.db.run(`DELETE FROM queue WHERE userID=1`, function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });        
    }

    // gets the details of specified channel
    getChannel(channelID) {
        this.db.get('SELECT title,channelID,last_updated,thumbnail FROM channels WHERE channelID=?', channelID, (err, row)=>{
            if (err) {
                console.error(`Error getting channel ${channelID}: ${err}`);
                reject(err);
            } else {
                resolve(row);
            }
        });
    }

    // gets videos for channel
    getVideosForChannel(id, max = 20) {
        return new Promise((resolve, reject)=>{
            this.db.all(`SELECT * FROM videos WHERE channelID=? ORDER BY publishedAt DESC LIMIT ?`, [id,max], (err, rows)=>{
                if (err) {
                    console.error(`Error getting videos for channel ${id}: ${err}`);
                    reject(err);
                } else {
                    resolve(rows);
                }
            });
        });
    }
};