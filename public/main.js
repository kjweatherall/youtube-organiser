//////////////////
///// CONSTS /////
//////////////////

// how many videos to initially show when viewing a category
const VIDEOS_PER_PAGE = 20;

// ids for pages
const PAGE_SUBS = 0;
const PAGE_CATEGORIES = 1;
const PAGE_CATEGORY = 2;

// ids for status

const STATUS_LOADING = 0;
const STATUS_LOADED = 1;
const STATUS_ERROR = 2;

/////////////////////
///// FUNCTIONS /////
/////////////////////

// converts time to local time and formats
function localTime(t) {
    let time = new Date(t).toString();
    return time.substring(0, time.indexOf(' GMT'));
}

// sends a POST request to the server
function sendData(url, body = { }) {
    return new Promise((resolve, reject)=>{
        fetch(`/api/v1/${url}`, {method:"post",headers:{"Content-Type":"application/json"},body:JSON.stringify(body)})
        .then(data=>data.json())
        .then(res=>{resolve(res);})
        .catch(err=>reject(err));
    });
}

//////////////////////
///// COMPONENTS /////
//////////////////////

// displays a video thumbnail and details
function Thumbnail(props) {
    if (!props.video || !props.channel) {
        console.error(`Missing data - Title: ${props.video?props.video.title:null}; Channel: ${props.channel?props.channel.title:null}`);
    }

    let className = "video-thumbnail";
    if (props.newVideo) {
        className += " new-video";
    }

    return (
        <div
            className={className}
            onClick={()=>{
                if (props.playVideo) {
                    props.playVideo(props.video.url, props.video.title, props.channel.title);
                }
            }}
        >
            <div>
                <img
                    src={props.video.thumbnail}
                    style={{width:"150px"}}
                />
            </div>
            <div style={{fontWeight:"bold"}} >
                {props.channel ? props.channel.title : null}
            </div>
            <div>
                {props.video.title}
            </div>
            <div>
                {localTime(props.video.publishedAt)}
            </div>
        </div>
    );
}

///////////////////
///// SIDEBAR /////
///////////////////

// category/sub-category link on sidebar
function NavLink(props) {
    // determine class name for element
    let elemClass = "page-link";
    if (props.target && (props.target.categoryID === props.category.categoryID)) {
        elemClass += " viewing";
    }
    
    // number of updates for category
    let total = 0,
        updates = props.updates[props.category.categoryID];
    if (updates) {
        total = updates.updates + updates.children;
    }

    // text to display
    let text = props.category.title;
    if (total > 0) {
        text += ` (${total})`;
    }

    return (
        <span
            onClick={_=>props.showCategory(props.category.categoryID)}
            className={elemClass}
        >
            {text}
        </span>
    );
}

// sidebar that shows categories among other things
function NavBar(props) {
    // get current category being looked at, if one, to know which link to emphasise
    let target = null;
    if (props.page === PAGE_CATEGORY) {
        target = props.categories.find(c=>c.categoryID === props.category);
    }

    return (
        <nav>
            <ul>
                <li className="page-link" onClick={()=>props.setPage(PAGE_SUBS)}>Subs</li>
                <li>
                    <div className="page-link" onClick={()=>props.setPage(PAGE_CATEGORIES)}>Categories</div>
                    <ul>
                        {props.categories.map(c=>(
                            c.parentID ? 
                                null : 
                                <li key={c.categoryID}>
                                    <NavLink
                                        category={c}
                                        target={target}
                                        updates={props.updates}
                                        showCategory={props.showCategory} 
                                    />
                                    <ul>
                                        {props.categories.map(s=>(
                                            (s.parentID === c.categoryID) ? 
                                                <li key={s.categoryID}>
                                                    <NavLink
                                                        category={s}
                                                        target={target}
                                                        updates={props.updates}
                                                        showCategory={props.showCategory}
                                                    />
                                                </li> :
                                                null
                                        ))}
                                    </ul>
                                </li>
                        ))}
                    </ul>
                </li>                
                <li>
                    <div>Actions</div>
                    <ul>                        
                        <li className="page-link" onClick={_=>props.forceUpdate()}>Update Videos</li>
                        <li className="page-link" onClick={_=>props.forceSubs()}>Update Subs</li>
                        <li className="page-link" onClick={_=>props.checkForUpdates()}>Check For Updates</li>
                    </ul>
                </li>
            </ul>
        </nav>
    );
}

/////////////////////////
///// SUBSCRIPTIONS /////
/////////////////////////

// a category displayed under a sub on the Subs page
function SubCategory(props) {
    // do nothing if not a subcategory of current category
    if (props.parentID !== props.category.parentID) {
        return null;
    }

    // if channel is included in this category
    let isIncluded = props.included[props.category.categoryID];
    
    return (
        <li className={isIncluded?'included':'excluded'} style={{cursor:"pointer"}}>
            <div onClick={_=>props.toggleChannelInCategory(props.category.categoryID, props.channel.channelID)}>{props.category.title}</div>
            {!props.parentID&&(
                <ul>
                    {props.categories.map(c=>{
                        if (c.parentID !== props.category.categoryID) {
                            return;
                        }

                        return (
                            <SubCategory
                                key={c.categoryID}
                                channel={props.channel}
                                category={c}
                                categories={props.categories}
                                parentID={props.category.categoryID}
                                included={props.included}
                                toggleChannelInCategory={props.toggleChannelInCategory}
                            />
                        );
                    })}
                </ul>
            )}
        </li>
    );
}

// channel displayed on subscription page
function Sub(props) {
    // highlight channel if it hasn't been added to any categories
    let bg;
    if (props.categorised[props.channel.channelID]) {
        bg = 'initial';
    } else {
        bg = 'orange';
    }

    return (
        <li>
            <div
                onClick={_=>props.selectChannel(props.channel.channelID)}
                style={{fontWeight:"bold",backgroundColor:bg}}
            >
                {props.channel.title}
            </div>
            {props.selected&&(
                <ul>
                    {props.categories.map(c=>{
                        if (c.parentID !== null) {
                            return null;
                        }

                        return (
                            <SubCategory
                                key={c.categoryID}
                                channel={props.channel}
                                category={c}
                                categories={props.categories}
                                parentID={null}
                                included={props.included}
                                toggleChannelInCategory={props.toggleChannelInCategory}
                            />
                        );
                    })}
                </ul>
            )}
        </li>
    );
}

// page that lists each subscription
class Subs extends React.Component {
    constructor(props) {
        super(props);

        // used to keep track of which categories a sub belongs to
        let included = { };
        props.categories.forEach(c=>{
            included[c.categoryID] = false
        });

        // used to keep track of which subs belong to no categories
        let categorised = { };
        props.subs.forEach(s=>{
            categorised[s.channelID] = (this.getCategoriesForChannel(s.channelID).length > 0);
        });

        // currently selected channel
        this.channel = null;

        this.state = {
            selected:null, // id of selected channel
            included:included, // categories the channel belongs to
            videos:[], // videos loaded for channel
            categorised:categorised, // if channels have been added to categories or not
            filter:"" // value that channels are being filtered by
        };

        this.selectChannel = this.selectChannel.bind(this);
        this.toggleChannelInCategory = this.toggleChannelInCategory.bind(this);
    }

    // returns a list of all categories a channel belongs to
    getCategoriesForChannel(id, exclude = [ ]) {
        return this.props.categories.filter(c=>!exclude.includes(c.categoryID) && c.channels.includes(id));
    }

    // selects the channel for the specified index
    selectChannel(id) {
        if (this.state.selected === id) {
            // selecting channel that was already selected deselects it
            this.setState({selected:null});
            this.channel = null;
        } else {
            let sub = this.props.subs.find(s=>s.channelID===id);
            if (!sub) {
                console.error(`Could not find sub with index of ${idx} for selecting.`);
                return;
            }

            // find each category that contains channel
            let included = this.state.included;
            for (let i in included) {
                let c = this.props.categories.find(cat=>cat.categoryID === +i);
                if (c) {
                    included[i] = c.channels.includes(id);
                }
            }

            this.setState({
                selected:id,
                included:included,
                videos:[]
            });

            // remember channel
            this.channel = sub;

            // fetch videos for channel and display
            fetch(`/api/v1/videos?id=${this.channel.channelID}&max=30`)
                .then(data=>data.json())
                .then(json=>{
                    if (!json.error) {
                        this.setState({videos:json.videos});
                    } else {
                        console.error(`Error getting videos: ${json.errorMsg}`);
                    }
                })
                .catch(err=>console.error(`Error fetching videos: ${err}`));
            
        }
    }

    // adds/removes channel from category
    toggleChannelInCategory(category, channel) {
        let included = this.state.included;
        let cats = this.state.categorised;

        if (included[category]) { // remove from category
            // update if channel belongs to any categories
            cats[channel] = this.getCategoriesForChannel(channel,[category]).length > 0;
            // remove channel from category
            this.props.removeChannelFromCategory(category, channel);
        } else { // add to category
            // set channel to belonging to at least 1 category
            cats[channel] = true;
            // add channel to category
            this.props.addChannelToCategory(category, channel);
        }

        // toggle channel belonging to category
        included[category] = !included[category];

        this.setState({
            included:included,
            categorised:cats
        });
    }

    render() {
        return (
            <div>
                <h1>Subscriptions</h1>
                <div style={{display:"grid",gridTemplateColumns:"250px auto"}}>
                    <div style={{borderRight:"1px solid black"}}>
                        <input
                            type="text"
                            style={{width:'98%'}}
                            value={this.state.filter}
                            onChange={e=>this.setState({filter:e.target.value})}
                        />
                        <ul style={{height:`${window.innerHeight * .9}px`,overflowY:"auto"}}>
                            {this.props.subs && this.props.subs.map((s)=>(
                                (this.state.filter.length > 0 && !s.title.toLowerCase().includes(this.state.filter.toLowerCase()) ? 
                                    null :
                                    <Sub
                                        key={s.channelID}
                                        channel={s}
                                        categories={this.props.categories}
                                        categorised={this.state.categorised}
                                        selectChannel={this.selectChannel}
                                        selected={this.state.selected===s.channelID}
                                        included={this.state.included}
                                        toggleChannelInCategory={this.toggleChannelInCategory}
                                    />
                                )
                            ))}
                        </ul>
                    </div>
                    {this.state.selected &&
                        <div style={{paddingLeft:"5px"}}>
                            <h3>{this.channel.title}</h3>
                            <div style={{display:"grid",gridTemplateColumns:"auto auto auto"}}>
                                {this.state.videos.map((v,i)=>(
                                    <Thumbnail key={v.url} idx={i} video={v} channel={this.channel} newVideos={0} playVideo={this.props.playVideo} />
                                ))}
                            </div>
                        </div>
                    }
                </div>
            </div>
        );
    }
}

//////////////////////
///// CATEGORIES /////
//////////////////////

// text box for adding subcategories
class AddBox extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value:"" // value of text box for category name
        };

        this.addCategory = this.addCategory.bind(this);
    }

    // adds the subcategory with the value from the state
    addCategory() {
        let val = this.state.value;
        this.props.addCategory(val, this.props.parentID);
        this.setState({value:""});
    }

    render() {
        return (
            <div>
                <input type="text" value={this.state.value} onChange={e=>this.setState({value:e.target.value})} />
                <button onClick={this.addCategory}>Add</button>
            </div>
        );
    }
}

// a category displayed under a subscription
function CategoriesCategory(props) {
    // do nothing if not a subcategory of current category
    if (props.parentID !== props.category.parentID) {
        return null;
    }

    // only user for main categories, as subcategories cannot have subcategories added, and thus aren't clickable
    let style = {display:"inline-block"};
    if (!props.parentID) {
        style.cursor = "pointer";
    }

    return (
        <div>
            <div style={style}>
                <span onClick={props.selectCategory?_=>props.selectCategory(props.category.categoryID):null}>
                    {props.category.title}
                </span>
                <span
                    style={{marginLeft:"10px",color:"red",cursor:"pointer"}}
                    onClick={_=>props.removeCategory(props.category.categoryID, props.parentID)}
                >
                    Remove
                </span>
            </div>
            {!props.parentID&&(
                <ul>
                    {props.categories.map(c=>{
                        if (c.parentID !== props.category.categoryID) {
                            return;
                        }

                        return (
                            <li key={c.categoryID}>
                                <CategoriesCategory 
                                    key={c.categoryID}
                                    category={c}
                                    categories={props.categories}
                                    parentID={props.category.categoryID}
                                    removeCategory={props.removeCategory}
                                    selectCategory={null}
                                />
                            </li>
                        );
                    })}
                </ul>
            )}
        </div>
    );
}

// the categories page
class Categories extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newCategory:"", // value of text box for adding new category
            selected:null,
            subcategory:""
        };

        this.selectCategory = this.selectCategory.bind(this);
        this.addCategory = this.addCategory.bind(this);
    }

    // adds a new category
    addCategory(title, parentID = null) {
        if (title) {
            this.props.addCategory(title, parentID);
            this.setState({newCategory:""});
        }
    }

    // selects a category to add a subcategory to
    selectCategory(id) {
        if (id === this.state.selected) {
            this.setState({
                selected:null,
                subcategory:""
            });
        } else {
            this.setState({selected:id});
        }
    }

    render() {
        return (
            <div>
                <h1>Categories</h1>
                <div>
                    <AddBox
                        addCategory={this.addCategory}
                        parentID={null}
                    />
                </div>
                <ul>
                    {this.props.categories.map(c=>{
                        if (c.parentID) {
                            return null;
                        }

                        return (
                            <li key={c.categoryID}>
                                <CategoriesCategory
                                    category={c}
                                    categories={this.props.categories}
                                    parentID={null}
                                    removeCategory={this.props.removeCategory}
                                    selectCategory={this.selectCategory}
                                />
                                {(this.state.selected === c.categoryID) && (
                                    <AddBox 
                                        addCategory={this.addCategory} 
                                        parentID={c.categoryID}
                                    />
                                )}
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}

////////////////////
///// CATEGORY /////
////////////////////

class Category extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newSub:"", // filter subs to add new one
            videos:[], // videos shown
            offset:0, // how many "pages" of videos have been loaded
            fetching:false // if videos are currently being fetched
        };

        // list of channels from among subs when filtered
        this.subs = [ ];
        // used to keep track of how many unwatched videos there are for each channel
        this.unwatched = { };
    }

    componentDidMount() {
        this.getVideos();
    }

    componentDidUpdate(prevProps) {
        // get videos if category changes
        if (prevProps.id !== this.props.id) {
            this.getVideos();
        }
    }

    // fetches list of videos from server
    // offset is how many videos past the most recent video the first video returned should be
    getVideos(offset = 0) {
        this.props.clearUpdates(this.props.id);

        let newState = {
            offset:offset,
            fetching:true
        };

        // if offset is 0, it is assumed we are just going to show the most recent videos, so we should clear
        // existing videos
        if (offset === 0) {
            newState.videos = [ ];
        }

        this.setState(newState);

        fetch(`/api/v1/category-videos?id=${this.props.id}&offset=${offset}&results=${VIDEOS_PER_PAGE}`).then(data=>data.json()).then(json=>{
            if (!json.error) {
                let newState = { fetching:false };

                // set videos or add to list depending on offset
                if (offset === 0) {
                    newState.videos = json.videos;
                } else {
                    let videos = this.state.videos;
                    newState.videos = videos.concat(json.videos);
                }

                this.setState(newState);
            } else {
                console.error(`Error getting videos for category (1): ${json.errorMsg}`);
            }
        })
        .catch(err=>console.error(`Error getting videos for category (2): ${err}`));
    }

    // filters subs for adding to channel
    filterSubs(val) {
        this.setState({newSub:val});

        this.subs = [ ];

        // if filter specified, display the first 5 results
        if (val) {
            for (let i = 0; i < this.props.subs.length && this.subs.length < 5; i++) {
                let s = this.props.subs[i];
                if (!this.props.channels.find(c=>c === s.channelID) && s.title.toLowerCase().includes(val.toLowerCase())) {
                    this.subs.push(s);
                }   
            }
        }
    }

    // adds a channel to the category
    addChannel(channel) {
        this.filterSubs("");
        this.props.addChannelToCategory(this.props.id, channel);
        this.getVideos();
    }

    // gets the next set of older videos
    loadMore() {
        this.getVideos(this.state.offset + VIDEOS_PER_PAGE);
    }

    render() {
        // if any channels in category has new videos
        let isUpdates = this.props.updates[this.props.id] && (this.props.updates[this.props.id].updates > 0 || this.props.updates[this.props.id].children > 0);
        // class for showing thumbnails, based on if video is showing
        let thmbClass = this.props.halved ? "thumbnails-mini" : "thumbnails";
        // which videos are new is determined by highlighting videos if the count for the channel is greater than zero and then reducing that value by 1, so we
        // copy the object with this data so we don't alter the original
        let unwatched = Object.assign({}, this.props.unwatched);

        return (
            <div>
                <h2>{this.props.title}</h2>
                <div>
                    Add:
                    <input type="text" value={this.state.newSub} onChange={e=>this.filterSubs(e.target.value)} />
                </div>
                <div>
                    {this.subs.map(s=>(
                        <div key={s.channelID} onClick={_=>this.addChannel(s.channelID)}>
                            <img src={s.thumbnail} style={{height:"30px"}} /> {s.title}
                        </div>
                    ))}
                </div>
                <div>
                    {this.props.channels.map(c=>{
                        let sub = this.props.subs.find(s=>s.channelID===c);
                        let thumbnail = (sub ? sub.thumbnail : null);

                        return <img
                            key={c}
                            src={thumbnail}
                            style={{height:"50px",margin:"3px"}}
                            onClick={_=>this.removeChannel(c)}
                            title={c}
                        />
                    })}
                </div>
                {(isUpdates) && <div style={{backgroundColor:"azure"}} onClick={_=>this.getVideos()}>REFRESH</div>}
                <div className={thmbClass}>
                    {this.state.videos.map((v,i)=>{
                            let channel = this.props.subs.find(s=>s.channelID===v.channelID);
                            let newVideo = false;

                            if (unwatched[v.channelID] > 0) {
                                unwatched[v.channelID]--;
                                newVideo = true;
                            }

                            return (
                                <Thumbnail                                
                                    key={v.url}
                                    idx={i}
                                    video={v}
                                    channel={channel}
                                    newVideos={this.props.newVideos}
                                    playVideo={this.props.playVideo}
                                    newVideo={newVideo}
                                />
                            )
                        })
                    }
                </div>
                <div onClick={_=>this.loadMore()} style={{backgroundColor:"azure",textAlign:"center",fontSize:"2em",cursor:"pointer"}}>{!this.state.fetching?"Load More":"Fetching..."}</div>
            </div>
        );
    }
}

////////////////////
///// MAIN APP /////
////////////////////

// queue of videos to watch
class Queue extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filter:"", // value for filtering queue
            nowPlaying:null, // details of currently playing video
            removeAfterWatch:true // if currently playing video should be removed when video changes
        };

        // video element object
        this.video = null;

        this.clearQueue = this.clearQueue.bind(this);
    }

    componentDidMount() {
        let queue = this.props.queue;
        if (queue.length > 0) {            
            let item = queue[queue.length - 1];
            this.playQueue(item.qID);
        }
    }

    // creates video object
    refreshVideo(id, autoplay = false) {
        if (!this.state.nowPlaying || id !== this.state.nowPlaying.id) {
            this.video = (
                <iframe 
                    className="player" 
                    width="100%" 
                    height="100%" 
                    src={`https://www.youtube.com/embed/${id}`} 
                    frameBorder="0" 
                    allowFullScreen="1" 
                    autoPlay={autoplay?"1":"0"}
                />
            );
        }
    }

    // empties the queue
    clearQueue() {
        this.setState({queue:[]});
                
        sendData('queue/clear')
        .catch(err=>console.log(err));
    }

    // plays a video in the queue
    playQueue(qID) {
        let vid = this.props.queue.find(q=>q.qID===qID);

        // do nothing if video is not found or video to play is already playing
        if (!vid || (this.state.nowPlaying && this.state.nowPlaying.id === vid.id)) {
            return;
        }

        // remove currently playing video if it's supposed to
        if (this.state.nowPlaying && this.state.removeAfterWatch) {
            this.props.removeFromQueue(this.state.nowPlaying.qID);
        }

        // change currently playing video
        this.setState({nowPlaying:vid,removeAfterWatch:true});
        this.refreshVideo(vid.id, true);

        return true;
    }

    // removes a video in the queue
    removeFromQueue(qID) {
        let vid = this.props.queue.find(v=>v.qID===qID);

        if (vid && this.state.nowPlaying && this.state.nowPlaying.id === vid.id && this.state.removeAfterWatch) {
            this.setState({
                removeAfterWatch:false
            });
        } else {
            this.props.removeFromQueue(qID);
        }
    }

    render() {
        return (
            <aside style={{width:(window.innerWidth * .4 + "px")}}>
                <div style={{height:(window.innerWidth * .4 / 16 * 9 + "px"),marginBottom:"10px"}}>
                    {this.video}
                </div>
                {(this.state.nowPlaying !== null) && (<div>{this.state.nowPlaying.title} - {this.state.nowPlaying.channel}</div>)}
                <div>
                    <input
                        type='text'
                        value={this.state.filter}
                        onChange={e=>this.setState({filter:e.target.value})}
                        style={{width:'100%'}}
                    />
                </div>
                <div className="queue">
                    <div
                        style={{textAlign:"center",cursor:"pointer"}}
                        onClick={this.clearQueue}
                    >
                        Clear
                    </div>
                    {this.props.queue.map((q)=>{
                        if (this.state.filter.length > 0) {
                            let filter = this.state.filter.toLowerCase();
                            if (!q.title.toLowerCase().includes(filter) &&
                                !q.channel.toLowerCase().includes(filter)) {
                                return null
                            }
                        }

                        let selected = (this.state.nowPlaying && this.state.nowPlaying.id === q.id);
                        let text = (selected && this.state.removeAfterWatch) ? "+" : "-";

                        return (
                            <div 
                                key={q.qID}
                                className={selected ? "active" : ""} 
                                style={{display:"grid",gridTemplateColumns:"auto 75px 30px"}}
                            >
                                <div onClick={_=>this.playQueue(q.qID)}>
                                    {q.title} - {q.channel}
                                </div>
                                <div>
                                    <a target="_blank" href={`https://www.youtube.com/watch?v=${q.id}`}>Open</a>
                                </div>
                                <div style={{fontSize:"2em"}} onClick={_=>this.removeFromQueue(q.qID)}>
                                    {text}
                                </div>
                            </div>
                        );
                    })}
                </div>
            </aside>
        );
    }
}

// main app
class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            status:STATUS_LOADING, // current status of app
            subs:null, // channels subscribed to
            categories:null, // 
            page:PAGE_SUBS, // current page
            category:-1, // current category being looked at
            lastUpload:null, // upload date/time of most recent video
            updates:{}, // new videos for each category
            newVideos:0, // total number of new videos
            queue:[], // current queue of videos
            nowPlaying:null, // details regarding currently playing video
            showQueue:false, // if queue should be showing
            unwatched:[], // collection of each
            allowLogout:true // if logout link is provided
        };

        // how many unwatched videos per channel
        this.unwatched = { };

        this.removeCategory = this.removeCategory.bind(this);
        this.removeFromQueue = this.removeFromQueue.bind(this);
        this.showCategory = this.showCategory.bind(this);

        setInterval(this.checkForUpdates.bind(this), 1000 * 60);
    }

    // get data when component is ready
    componentDidMount() {
        fetch('/api/v1/user')
        .then(data=>data.json())
        .then(json=>{
            if (!json.error) {
                this.setState({
                    status:STATUS_LOADED,
                    subs:json.data.subs,
                    categories:json.data.categories,
                    lastUpload:json.data.lastUpload,
                    queue:json.data.queue,
                    allowLogout:json.data.allowLogout
                });

                this.applyUpdates(json.data.updates);
            } else {
                console.error(`Error: ${json.errorMsg}`);
            }
        })
        .catch(err=>{
            console.error(`Error: ${err}`);
            this.setState({status:STATUS_ERROR});
        });
    }

    // fetch any updates
    checkForUpdates() {
        fetch(`/api/v1/user/updates?after=${this.state.lastUpload}`)
        .then(data=>data.json())
        .then(json=>{
            if (!json.error) {
                if (json.lastUpload) {
                    this.setState({lastUpload:json.lastUpload});
                }

                this.applyUpdates(json.updates);
            } else {
                console.error(`Error getting updates (1): ${json.errorMsg}`);
            }
        })
        .catch(err=>console.error(`Error getting updates (2): ${err}`));
    }

    // sets the page
    setPage(idx) {
        this.setState({
            page:idx,
            category:-1
        });
    }

    // shows a category
    showCategory(categoryID) {
        // calculate number of unwatched videos in category
        let updates = this.state.updates[categoryID];
        let total = 0;

        if (updates) {
            total = updates.updates + updates.children;

            // collects the number of unwatched videos for category and its subcategories
            let cat = this.state.categories.find(c=>c.categoryID===categoryID);
            if (cat) {
                this.unwatched = { };

                // get all channels that belong to category
                let chans = cat.channels.slice(0);

                // get all channels that belong to subcategories of this category
                if (!cat.parentID) {
                    for (let c of this.state.categories) {
                        if (c.parentID === categoryID) {
                            chans.push.apply(chans, c.channels);
                        }
                    }
                }

                // store number number of unwatched videos
                for (let c of chans) {
                    let chan = this.state.unwatched.find(u=>u.channelID===c);
                    if (chan) {
                        this.unwatched[c] = chan.unwatched || 0;
                    }
                }
            }
        }

        // clears count of unwatched videos for category and its subcategories
        this.clearUpdates(categoryID);
        
        this.setState({
            page:PAGE_CATEGORY,
            category:categoryID,
            newVideos:total
        });
    }

    // adds a new category with an optional parent
    addCategory(title, parentID = null) {
        fetch('/api/v1/categories/add', {method:"post",headers:{"Content-Type":"application/json"},body:JSON.stringify({title:title,parentID:parentID})})
        .then(data=>data.json())
        .then(json=>{
            if (!json.error) {
                // add new category to state; wait until now so server has confirmed it was added
                let categories = this.state.categories;
                categories.push(json.category);
                categories.sort((a,b)=>a.title.localeCompare(b.title));
                this.setState({categories:categories});
            } else {
                console.error(`Error adding category (1): ${json.errorMsg}`);
            }
        })
        .catch(err=>console.error(`Error adding category (2): ${err}`));
    }

    // removes a category
    removeCategory(categoryID) {
        fetch('/api/v1/categories/remove', {method:"post",headers:{"Content-Type":"application/json"},body:JSON.stringify({id:categoryID})})
        .then(data=>data.json())
        .then(json=>{
            if (!json.error) {
                let categories = this.state.categories;
                
                // remove category from state
                for (let i = 0; i < categories.length; i++) {
                    if (categories[i].categoryID === categoryID) {
                        categories.splice(i, 1);
                        break;
                    }
                }
                
                this.setState({categories:categories});
            } else {
                console.error(`Error removing category: ${json.errorMsg}`);
            }
        })
        .catch(err=>{
            console.error(`Error calling API to remove category: ${err}`);
        });
    }

    // adds a channel to a category
    addChannelToCategory(categoryID, channelID) {
        fetch('/api/v1/categories/add-channel', {method:"post",headers:{'Content-Type':'application/json'},body:JSON.stringify({id:categoryID,channel:channelID})})
        .then(data=>data.json())
        .then(json=>{
            if (json.error) {
                console.error(`Error adding channel to category (1): ${json.errorMsg}`);
            } else {
                // add channel to category in state
                let categories = this.state.categories;
                let c = categories.find(c=>c.categoryID === categoryID);
                if (c) {
                    c.channels.push(channelID);
                    this.setState({categories:categories});
                } else {
                    console.error(`Cannot find category ${categoryID}`);
                }
            }
        })
        .catch(err=>{
            console.error(`Error adding channel to category (2): ${err}`);
        });
    }

    // remvoes a channel from a category
    removeChannelFromCategory(categoryID, channelID) {
        fetch('/api/v1/categories/remove-channel', {method:"post",headers:{'Content-Type':'application/json'},body:JSON.stringify({id:categoryID,channel:channelID})})
        .then(data=>data.json())
        .then(json=>{
            if (json.error) {
                console.error(`Error adding channel to category (1): ${json.errorMsg}`);
            } else {
                // remove channel from category in state
                let categories = this.state.categories;
                let c = categories.find(c=>c.categoryID === category);
                if (c) {
                    for (let i = 0; i < c.channels.length; i++) {
                        if (c.channels[i] === channel) {
                            c.channels.splice(i, 1);
                            break;
                        }
                    }

                    this.setState({categories:categories});
                } else {
                    console.error(`Cannot find category ${category}`);
                }
            }
        })
        .catch(err=>{
            console.error(`Error adding channel to category (2): ${err}`);
        });
    }
    
    // enqueues a video with the specified id, title and channel
    playVideo(id, title, channel) {
        sendData('queue/add', {videoID:id})
        .then(res=>{
            if (res.error) {
                console.error(`Error: ${res.errorMsg}`);
                alert(`Error adding video to queue on server`);
            } else {
                // change to state; make sure queue shows
                let newState = {showQueue:true};
                let queue = this.state.queue;

                // add video data to queue
                let vid = {id:id,title:title,channel:channel,qID:res.data.qID};                
                queue.push(vid);
                newState.queue = queue;

                this.setState(newState);
            }
        })
        .catch(err=>console.error(`Error pushing to queue: ${err}`));
    }

    // stores the count of unwatched videos for each channel
    applyUpdates(newUpdates) {
        let updates = { };

        // calculate unwatched for each category
        this.state.categories.forEach(cat=>{
            // get total count for all channels in category
            for (let chan of cat.channels) {
                let nu = newUpdates.find(c=>c.channelID===chan);
                if (nu) {
                    if (!updates[cat.categoryID]) {
                        updates[cat.categoryID] = {
                            updates:0,
                            children:0
                        };
                    }

                    if (updates[cat.categoryID]) {
                        updates[cat.categoryID].updates += (nu.unwatched || 0);
                    }
                }
            }

            // if subcategory, add unwatched count to parent's children count
            if (cat.parentID) {
                if (!updates[cat.parentID]) {
                    updates[cat.parentID] = {
                        updates:0,
                        children:0
                    };
                }

                if (updates[cat.categoryID]) {
                    updates[cat.parentID].children += updates[cat.categoryID].updates;
                }
            }
        });
        
        this.setState({
            updates:updates,
            unwatched:newUpdates
        }, ()=>this.updateTotal());
    }

    // update unwatched count for document title
    updateTotal() {
        let total = 0;
        for (let cat of this.state.categories) {
            for (let chan of cat.channels) {
                let uw = this.state.unwatched.find(u=>u.channelID===chan);
                if (uw) {
                    total += (uw.unwatched || 0);
                }
            }
        }

        let title = "YouTube Organiser";
        if (total) {
            title += ` (${total})`;
        }
        document.title = title;
    }

    // clears the number of unwatched videos for a category
    clearUpdates(categoryID) {
        let updates = this.state.updates;
        let unwatched = this.state.unwatched;
        
        if (updates[categoryID]) {
            // clear unwatched video count for children
            this.state.categories.forEach(c=>{
                if (c.parentID === categoryID) {
                    updates[c.categoryID] = {
                        updates:0,
                        children:0
                    };

                    c.channels.forEach(chan=>{
                        let uw = unwatched.find(u=>u.channelID===chan);
                        if (uw) {
                            uw.unwatched = 0;
                        }
                    });
                }
            });

            // if subcategory, decrease parent's subcategory unwatched video count
            let c = this.state.categories.find(c=>c.categoryID === categoryID);
            if (c && c.parentID) {
                updates[c.parentID].children -= updates[categoryID].updates;
            }

            // clear unwatched count
            updates[categoryID] = {
                updates:0,
                children:0
            };

            c.channels.forEach(chan=>{
                let uw = unwatched.find(u=>u.channelID===chan);
                if (uw) {
                    uw.unwatched = 0;
                }
            });

            // update page title
            this.updateTotal();
        }

        this.setState({updates:updates,unwatched:unwatched});

        fetch('/api/v1/updates/clear', {method:"post","headers":{"Content-Type":"application/json"},body:JSON.stringify({id:categoryID})})
        .then(data=>data.json())
        .then(json=>{
            if (json.error) {
                console.error(`Error clearing updates: ${json.errorMsg}`);
            }
        })
        .catch(err=>{
            console.error(`Error sending request to clear updates: ${err}`);
        });
    }

    // forces the server to check for new videos
    forceUpdate() {
        fetch(`/api/v1/force-update`);
    }

    // forces server to check for new subs
    forceSubs() {
        fetch('/api/v1/subscriptions/force')
        .then(data=>data.json())
        .then(json=>{
            if (!json.error) {
                this.setState({subs:json.subs});
            } else {
                console.error(`Error getting subs (forced): ${err}`);
            }
        })
        .catch(err=>{
            console.error(`Error fetching subs (forced): ${err}`);
        });
    }

    // shows queue
    showQueue() {
        this.setState({showQueue:true});
    }

    // hides queue
    hideQueue() {
        this.setState({showQueue:false});
    }

    // toggles queue
    toggleQueue() {
        this.setState({showQueue:!this.state.showQueue});
    }

    // removes a video from the queue
    removeFromQueue(qID) {
        let queue = this.state.queue;

        for (let i = Math.min(qID, queue.length - 1); i >= 0; i--) {
            if (queue[i].qID === qID) {
                queue.splice(i, 1);
                break;
            }
        }

        this.setState({queue:queue});

        sendData('queue/remove', {qID:qID});
    }

    render() {
        let page = null;

        // TODO: Move this out of the render function! Likely very inefficient
        if (this.state.status === STATUS_LOADED) {
            switch (this.state.page) {
                case PAGE_SUBS:
                    page = <Subs
                        subs={this.state.subs}
                        categories={this.state.categories}
                        addChannelToCategory={(category,channel)=>this.addChannelToCategory(category,channel)}
                        removeChannelFromCategory={(category,channel)=>this.removeChannelFromCategory(category,channel)}
                        playVideo={(id,title,channel)=>this.playVideo(id,title,channel)}
                    />;
                    break;
                case PAGE_CATEGORIES:
                    page = <Categories
                        categories={this.state.categories}
                        addCategory={(title,parent)=>this.addCategory(title,parent)}
                        removeCategory={this.removeCategory}
                    />;
                    break;
                case PAGE_CATEGORY:
                    let c = this.state.categories.find(c=>c.categoryID===this.state.category);
                    if (c) {
                        let channels = c.channels.slice();
                        this.state.categories.forEach(s=>{
                            if (s.parentID === c.categoryID) {
                                channels = channels.concat(s.channels);
                            }
                        });

                        channels.sort((a,b)=>{
                            return a.localeCompare(b);
                        });

                        for (let i = 1; i < channels.length; i++) {
                            while (channels[i] === channels[i - 1]) {
                                channels.splice(i, 1);
                            }
                        }

                        page = <Category
                            id={c.categoryID}
                            title={c.title}
                            channels={channels}
                            subs={this.state.subs}
                            addChannelToCategory={(channel,category)=>this.addChannelToCategory(channel,category)}
                            removeChannelFromCategory={(channel,category)=>this.removeChannelFromCategory(channel,category)}
                            playVideo={(id,title,channel)=>this.playVideo(id,title,channel)}
                            updates={this.state.updates}
                            clearUpdates={category=>this.clearUpdates(category)}
                            newVideos={this.state.newVideos}
                            halved={this.state.showQueue}
                            unwatched={this.unwatched}
                        />
                    }
                    break;
            }
        }

        // class name content area
        let contentClass = 'content ' + ((this.state.showQueue) ? 'halved' : 'full');

        return (
            this.state.status === STATUS_LOADING ? (
                <div>Loading...</div> 
            ) : 
            this.state.status === STATUS_ERROR ? (
                <div>Error</div> 
            ) : (
                <div className='page'>
                    <NavBar
                        category={this.state.category}
                        page={this.state.page}
                        categories={this.state.categories}
                        setPage={idx=>this.setPage(idx)}
                        showCategory={this.showCategory}
                        updates={this.state.updates}
                        forceUpdate={()=>this.forceUpdate()}
                        forceSubs={()=>this.forceSubs()}
                        checkForUpdates={()=>this.checkForUpdates()}
                    />
                    <div className={contentClass}>
                        <div style={{display:"inline-block",position:"fixed",right:0,top:0}}>
                            <span style={{cursor:"pointer"}} onClick={_=>this.toggleQueue()}>{(this.state.showQueue ? "Hide" : "Show") + " Queue"}</span>
                            {this.state.allowLogout&&<a href="/logout">Logout</a>}
                        </div>
                        <main>
                            {page}
                        </main>
                        { this.state.showQueue &&
                            <Queue
                                queue={this.state.queue}
                                removeFromQueue={this.removeFromQueue}
                            />
                        }
                    </div>
                </div>
            )
        );
    }
}

ReactDOM.render(
    <App />,
    document.querySelector('#app')
);